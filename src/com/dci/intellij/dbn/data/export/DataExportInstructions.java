package com.dci.intellij.dbn.data.export;

import com.dci.intellij.dbn.common.state.PersistentStateElement;
import com.dci.intellij.dbn.common.util.Cloneable;
import lombok.Data;
import org.jdom.Element;

import java.io.File;
import java.nio.charset.Charset;

import static com.dci.intellij.dbn.common.options.setting.SettingsSupport.*;

@Data
public class DataExportInstructions implements PersistentStateElement, Cloneable {
    private boolean createHeader = true;
    private boolean quoteValuesContainingSeparator = true;
    private boolean quoteAllValues = false;
    private String valueSeparator;
    private String fileName;
    private String fileLocation;
    private Scope scope = Scope.GLOBAL;
    private Destination destination = Destination.FILE;
    private DataExportFormat format = DataExportFormat.EXCEL;
    private String baseName;
    private Charset charset = Charset.defaultCharset();

    public File getFile() {
        return new File(fileLocation, fileName);
    }

    public enum Scope{
        GLOBAL,
        SELECTION
    }

    public enum Destination{
        FILE,
        CLIPBOARD
    }

    @Override
    public DataExportInstructions clone() {
        return PersistentStateElement.cloneElement(this, new DataExportInstructions());
    }

    /***********************************************
     *            PersistentStateElement           *
     ***********************************************/
    @Override
    public void writeState(Element element) {
        Element child = new Element("export-instructions");
        element.addContent(child);

        setBoolean(child, "create-header", createHeader);
        setBoolean(child, "quote-values-containing-separator", quoteValuesContainingSeparator);
        setBoolean(child, "quote-all-values", quoteAllValues);
        setString(child, "value-separator", valueSeparator);
        setString(child, "file-name", fileName);
        setString(child, "file-location", fileLocation);
        setString(child, "scope", scope.name());
        setString(child, "destination", destination.name());
        setString(child, "format", format.name());
        setString(child, "charset", charset.name());
    }

    @Override
    public void readState(Element element) {
        Element child = element.getChild("export-instructions");
        if (child != null) {
            createHeader = getBoolean(child, "create-header", createHeader);
            quoteValuesContainingSeparator = getBoolean(child, "quote-values-containing-separator", quoteValuesContainingSeparator);
            quoteAllValues = getBoolean(child, "quote-all-values", quoteAllValues);
            valueSeparator = getString(child, "value-separator", valueSeparator);
            fileName = getString(child, "file-name", fileName);
            fileLocation = getString(child, "file-location", fileLocation);
            scope = Scope.valueOf(getString(child, "scope", scope.name()));
            destination = Destination.valueOf(getString(child, "destination", destination.name()));
            format = DataExportFormat.valueOf(getString(child, "format", format.name()));
            charset = Charset.forName(getString(element, "charset", charset.name()));
        }
    }
}
